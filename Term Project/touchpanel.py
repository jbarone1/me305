''' @file touchpanel.py
    @brief Touch panel driver used to locate  touch
    @details The touch pannel is a resistive touch pannel which can only handle 
             a single input. The touch pannel can be considered as a pair of 
             potentiometers with common wipers that are adjusted by the 
             horizontal and vertical contact point on the pannel. The location 
             of the contact point changes the voltage divider ratio. We can 
             use the ratio to find the X and Y location as well as if there is 
             contact.
    Source code available here: 
    https://bitbucket.org/jbarone1/me305/src/master/Term%20Project/
    @author Jack Barone
    @author Ryker Bullis
    @date December 7, 2021
'''
import utime
import pyb
from ulab import numpy as np

class TouchPanel:
    ''' @brief Driver used to scan for location of touch on touch pannel
        @details Driver is able to scan for a single location in less than 
        500us. Driver scans all in less than 1500us.
    '''
    def __init__ (self,pinA0,pinA7,xp,xm,yp,ym):
        ''' @brief Initializes touch pannel
            @details 
            @param pinA0    Pin A0 used for ADC_ym and ADC_cont
            @param pinA7    Pin A7 used for ADC_xm
            @param xp       touchpanel pinout xp
            @param xm       touchpanel pinout xm
            @param yp       touchpanel pinout yp
            @param ym       touchpanel pinout ym

        '''
        ## PinA0 used for scanning
        self.pinA0 = pinA0 
        ## PinA7 used for scanning
        self.pinA7 = pinA7
        ## xp 
        self.xp = xp
        ## xm
        self.xm = xm
        ## yp
        self.yp = yp 
        ## ym
        self.ym = ym
        
    def scanX(self):
        ''' @brief Sets pin modes and scans for y location
            @return X location [mm]
        '''
        self.xp.init(mode=pyb.Pin.OUT_PP)
        self.xm.init(mode=pyb.Pin.OUT_PP)
        self.xp.high()
        self.xm.low()
        self.yp.init(mode=pyb.Pin.IN)
        self.ym.init(mode=pyb.Pin.IN)
        utime.sleep_us(4)
        ADC_xm = pyb.ADC(self.pinA0)
        xm_val = ADC_xm.read()
        x = xm_val*self.Kxx + self.Xo
        #stop = utime.ticks_us()
        #t = utime.ticks_diff(stop,start)
        return(x)
    
    def scanY(self):
        ''' @brief Sets pin modes and scans for y location 
            @return Y location [mm]
        '''
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.ym.init(mode = pyb.Pin.OUT_PP)
        self.yp.high() 
        self.ym.low() 
        self.xp.init(mode=pyb.Pin.IN)
        self.xm.init(mode=pyb.Pin.IN)
        utime.sleep_us(4)
        ADC_ym = pyb.ADC(self.pinA7)
        ym_val = ADC_ym.read()
        y = ym_val*self.Kyy + self.Yo
        return(y)
    
    def scanADC_X(self) :
        ''' @brief Sets pin modes and gets raw ADC_X values
            @return Xm_val 
        '''
        self.xp.init(mode=pyb.Pin.OUT_PP)
        self.xm.init(mode=pyb.Pin.OUT_PP)
        self.xp.high()
        self.xm.low()
        self.yp.init(mode=pyb.Pin.IN)
        self.ym.init(mode=pyb.Pin.IN)
        utime.sleep_us(4)
        ADC_xm = pyb.ADC(self.pinA0)
        xm_val = ADC_xm.read()
        return(xm_val)
    
    def scanADC_Y(self):
        ''' @brief sets pin modes and gets raw ADC_Y values
            @return ym_val 
        '''
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.ym.init(mode = pyb.Pin.OUT_PP)
        self.yp.high() 
        self.ym.low() 
        self.xp.init(mode=pyb.Pin.IN)
        self.xm.init(mode=pyb.Pin.IN)
        utime.sleep_us(4)
        ADC_ym = pyb.ADC(self.pinA7)
        ym_val = ADC_ym.read()
        return(ym_val)
    
    def scanZ(self):
        ''' @brief Sets pin modes and scans for Z location/contact
            @return Boolean contact variable
        '''
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.xm.init(mode = pyb.Pin.OUT_PP)
        self.yp.high()
        self.xm.low()
        self.xp.init(mode = pyb.Pin.IN)
        self.ym.init(mode = pyb.Pin.IN)
        utime.sleep_us(4)
        ADC_cont = pyb.ADC(self.pinA0)
        cont_val = ADC_cont.read() 
        if 40 <= cont_val <= 4055:
            contact = False
        else:
            contact = True
        return(contact)
    
    def scanXY(self): 
        ''' @brief Gets ADC_X and ADC_Y values, and applies calib. constants
            @return Touple of X and Y
        '''
        ADC_x = self.scanADC_X()
        ADC_y = self.scanADC_Y()
        
        X = self.Kxx*ADC_x + self.Kxy*ADC_y + self.Xo
        Y = self.Kyx*ADC_x + self.Kyy*ADC_y + self.Yo
        
        return(X,Y)
    def scanALL(self):
        ''' @brief Runs scanX(), scanY(), and scanZ()
            @return Touple of x and y loaction, boolean contact 
        '''
        start = utime.ticks_us()
        x = self.scanX()
        y = self.scanY()
        z = self.scanZ()
        stop = utime.ticks_us()
        t = utime.ticks_diff(stop,start)
        return(x, y, z, t)
    
    
    def calibration(self):
        ''' @brief Runs calibration process
            @details Physically touch on the panel in this order:
                     (0,0),(80,40),(-80,40),(-80,-40),(-80,-40)
            @return Calibration values
        '''
        coord_pos = np.array([[0,0],
                                [80,40],
                                [-80,40],
                                [-80,-40],
                                [80,-40]])
        ADC_meas = np.ones((5,3))
        n = 0
        print('touch center and all 4 corners. ')
        while True :
            utime.sleep_ms(300)
            Z = self.scanZ() 
            if Z == True:
                X = self.scanADC_X()
                Y = self.scanADC_Y()
                ADC_meas[n] = [X, Y, 1]
                print('Point: {} '.format(n))
                utime.sleep(1)
                n += 1
            if n == 5:
                print('Calibration complete')
                break
        ADC_tran = ADC_meas.transpose()
        step1 = np.linalg.inv(np.dot(ADC_tran,ADC_meas))
        step2 = np.dot(step1,ADC_tran)
        Beta = np.dot(step2,coord_pos)
        cal_values = (Beta[0,0], Beta[1,0], Beta[0,1], Beta[1,1], Beta[2,0], Beta[2,1])
        self.writeConstants(cal_values)   
        return(cal_values)
        
    def writeConstants(self,cal_values):
        ''' @brief grabs Calibration values from calibration() 
            @return grabs Values from cal_values and writes to each K value 
            which are used in all scans
            @param cal_values Srray of calibration constants.
        '''
        self.Kxx = cal_values[0]
        self.Kxy = cal_values[1]
        self.Kyx = cal_values[2]
        self.Kyy = cal_values[3]
        self.Xo  = cal_values[4]
        self.Yo  = cal_values[5]
    