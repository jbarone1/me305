'''@file task_motor.py
   @brief Responsible for motor control
   @details    Controls motor by setting duty cycle
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   @author Ryker Bullis
   @author Jack Barone
   @date December 7, 2021
'''

class Task_Motor:   
    '''@brief Responsible setting duty cycle of motor
    '''
    def __init__(self,motor_drv,duty,motor): 
        '''@brief Constructs motor task
           @details Task used to control the motor
           @param motor_drv     Constructor which calls motor driver
           @param duty          Duty cycle of the motor
           @param motor         Method in motor driver
        '''
        ## Constructor calling method of DRV8847
        self.motor = motor
        ## Duty cycle of the motor
        self.duty = duty
        ## Calls motor driver, set up in main
        self.motor_drv = motor_drv

    def run(self):
        '''@brief
        '''
        self.motor.set_duty(int(self.duty.read()))

    def transition_to(self, new_state): 
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        ##Defines state as new_state
        self.state = new_state 
