''' @file task_imu.py
    @brief      
    @details    
    @author Ryker Bullis
    @author Jack Barone
    @date December 1, 2021
'''

import imu
import pyb
import os
import time

mode_config = 0x00
mode_NDOF = 0x0C
mode_NDOF_OFF = 0x0B

S0_INIT     = 0
S1_EULER    = 1

class Task_IMU:
    ''' @brief      
        @details    
    '''

    def __init__(self,panel_vector):
        ''' @brief Initializes IMU task
            @param 
        '''
        self.i2c = pyb.I2C(1,pyb.I2C.MASTER)
        self.IMU_driver = imu.IMU(self.i2c)
        self.i2c.mem_write(0x05,0x28,0x3B)      # Unit selection
        self.IMU_driver.mode(mode_NDOF_OFF)     # Set mode to NDOF
        ## Sets the initial state of the FSM
        self.state = S0_INIT
        self.panel_vector = panel_vector
        self.run_calibration()
        
    def run_calibration(self):
        ''' @brief
        '''
        status = self.IMU_driver.get_status()
        #print(status)
            
        filename = "IMU_cal_coeffs.txt"
        if filename in os.listdir():
            print('Fetching calibration data...')
            self.IMU_driver.mode(mode_config)
            with open(filename,'r') as f:
                imu_data_string = f.readline()
                imu_cal_values = [int(cal_value) for cal_value in imu_data_string.strip().split(',')]
                self.IMU_driver.write_constants(imu_cal_values)
            self.IMU_driver.mode(mode_NDOF)
            #print('done')
            
        else:
            while status[0] + status[1] + status[2] + status[3] != 12:
                status = self.IMU_driver.get_status()
                print(status)
            print("Fully Calibrated")
            cal_data_string = self.IMU_driver.get_constants()
            print(cal_data_string)
            with open(filename, 'w') as f:
                (K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,K11,K12,K13,K14,K15,K16,K17,K18,K19,K20,K21,K22) = self.IMU_driver.get_constants()
                f.write(f"{K1},{K2},{K3},{K4},{K5},{K6},{K7},{K8},{K9},{K10},{K11},{K12},{K13},{K14},{K15},{K16},{K17},{K18},{K19},{K20},{K21},{K22}\r\n")
                
        
    def run(self):
        ''' @brief IMU calibration
        '''
        euler = self.IMU_driver.read_euler()
        vel = self.IMU_driver.read_angvel()
            
        # ( theta_x , theta_dot_x , theta_y , theta_dot_y )
        self.panel_vector[0].write(-euler[1])
        self.panel_vector[1].write(-vel[1])
        self.panel_vector[2].write(-euler[2])
        self.panel_vector[3].write(-vel[2])

    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state














