'''@file task_user.py
   @brief User interface for encoder and motor operation
   @details    Implements a finite state machine for the user task. Prompts
               the user for inputs and returns data where applicable. 
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   @author Ryker Bullis
   @author Jack Barone
   @date December 4, 2021
'''
import utime
import pyb
import sys

S0_INIT             = 0
S1_WAIT             = 1
S2_COLLECT          = 2

class Task_User:
    ''' @brief User UI for TERM Project
        @details    Implements a finite state machine for the user task. Cycles 
                    through collection, printing, and additional states. 
    '''
    
    def __init__(self, run_flag):
        
        self.state = S0_INIT
        self.ser = pyb.USB_VCP()  
        self.dataPeriod = 100_000
        self.next_time = utime.ticks_add(utime.ticks_us(), self.dataPeriod) 
        self.num_st = ''
        self.run_flag = run_flag
    def run(self):
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0): 
            if self.state == S0_INIT:
                print('Press b to balance: \n',
                      'Press s to stop: \n')
                self.transition_to(S1_WAIT)
        
        elif self.state == S1_WAIT:
            if self.ser.any():
                    
                ## Variable representing keyboard inputs to advance FSM
                char_in = self.ser.read(1).decode()

                # Zero the position of encoder 1
                if(char_in == 'b'):
                    self.run_flag.write(True)
                    print('BOOM...')
                if(char_in == 's'):
                    self.run_flag.write(False)
                    print('OKURR...')
                    self.transition_to(S0_INIT)
                
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state
    
    def user_input(self):
        '''@brief      Deals with user input 
           @details    Reads one character at a time from user
        '''
        # String which is printed into the GUI window
        number = None
        if self.ser.any():
            char_in = self.ser.read(1).decode()
            if char_in.isdigit():
                self.num_st += char_in
                sys.stdout.write(char_in)
                return None
            elif char_in == '-':
                if len(self.num_st) == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif ord(char_in) == 127: 
                if len(self.num_st) > 0: 
                    self.num_st = self.num_st[:-1]
                    sys.stdout.write('\b \b') 
                    sys.stdout.write('')
                    return None
            elif char_in == '.':
                if self.num_st.count('.') == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif char_in == '\r':
                print(' \n')
                number = float(self.num_st)
                self.num_st = ''
                return(number)