var indexSectionsWithContent =
{
  0: "_bcdeghmpst",
  1: "em",
  2: "h",
  3: "ehm",
  4: "_gs",
  5: "cdmpt",
  6: "b"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

