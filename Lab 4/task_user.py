'''@file task_user.py
   @brief User interface for encoder and motor operation
   @details    Implements a finite state machine for the user task. Prompts
               the user for inputs and returns data where applicable. 
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   @author Ryker Bullis
   @author Jack Barone
   @date November 15, 2021
'''
import utime
import pyb
import array
import sys

S0_INIT             = 0
S1_WAIT             = 1
S2_COLLECT          = 2
S3_COLLECT          = 3
S4_PRINT            = 4
S5_PRINT            = 5
S6_COLLECT_CL       = 6
S7_COLLECT_CL       = 7
S8_PRINT_CL         = 8
S9_PRINT_CL         = 9
S10_GET_KP          = 10
S11_GET_KP          = 11
S12_GET_VEL         = 12
S13_GET_VEL         = 13
S14_DUTY            = 14
S15_DUTY            = 15

class Task_User:
    ''' @brief User interface for encoder and motor operation
        @details    Implements a finite state machine for the user task. Cycles 
                    through collection, printing, and additional states. 
    '''
    
    def __init__(self,dataPeriod,zero_flag,enc_pos,enc_delta,
                 fault_flag,duty,closedloop_flag, Kp, omega_ref, dbg=True):
        ''' @brief                  Constructs a user task
            @details                The task is implemented as a finite state
                                    machine.
            @param dataPeriod       Period for which data points are collected
            @param zero_flag        Shared object which resets encoder
            @param enc_pos          Shared queue for encoder position
            @param enc_delta        Shared queue for encoder delta
            @param fault_flag       Flag which indicates when a fault has occurred
            @param duty             Motor duty cycle
            @param closedloop_flag  Boolean flag which runs the closed loop controller
            @param Kp               Proportional gain 
            @param omega_ref        Target motor speed
            @param dbg              Handles serial port error messages
        '''

        ## Set the initial state of the FSM
        self.state = S0_INIT       
        ## The period for checking commands
        self.dataPeriod = dataPeriod        
        ## Serial port to receive keystrokes in PuTTy
        self.ser = pyb.USB_VCP()        
        ## The time of the next FSM run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.dataPeriod)        
        ## Length of time to collect data
        self.dataLength = 30_000_000/dataPeriod        
        ## Create an empty list for the encoder 1 position to record in
        self.dataENC1 = array.array('f',300*[0])
        ## Create an empty list for the encoder 2 position to record in
        self.dataENC2 = array.array('f',300*[0])
        ## Create an empty array for motor 1 velocity
        self.velENC1 = array.array('f',300*[0])
        ## Create an empty array for motor 2 velocity
        self.velENC2 = array.array('f',300*[0])
        ## Create an empty array for the encoder time to record in
        self.dataTime = array.array('f',300*[0])
        ## Duty cycle for motor 1
        self.duty_data1 = array.array('f',300*[0])
        ## Duty cycle for motor 2
        self.duty_data2 = array.array('f',300*[0])
        ## Define the zero flag for encoder 1
        self.zero_flag = zero_flag
        ## Current position of the encoder
        self.enc_pos = enc_pos
        ## Delta of the encoder
        self.enc_delta = enc_delta
        # Create time step tracker variable
        self.timeEntry = 0
        ## Create fault flag
        self.fault_flag = fault_flag
        ## Duty cycle for motors
        self.duty = duty
        ## Array index for assigning data
        self.arrayIDX = 0
        ## Flag to run closedloop motor controller
        self.closedloop_flag = closedloop_flag
        ## Proportional gain used in motor controller
        self.Kp = Kp 
        ## Target motor velocity
        self.omega_ref = omega_ref
        ## Instantiates string used to handle user input
        self.num_st = ''
        
    def run(self):
        '''@brief Runs an iteration of the FSM
           @details Switches between waiting, collecting data, and printing.
        '''
        
        ## Current time using the utime counter
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0): 
            if self.state == S0_INIT:
                print(' Press z | Z to zero encoder 1 | 2. \n',
                      'Press p | P to print encoder 1 | 2 position.\n',
                      'Press d | D to print the delta for encoder 1 | 2.\n',
                      'Press g | G to collect 30 seconds of encoder 1 | 2 data.\n',
                      'Press m | M to enter motor 1 | 2 duty cycle. \n',
                      'Press s to stop data collection early.\n',
                      'Press 1 | 2 to perform step response for motor 1 | 2. \n',)
                self.transition_to(S1_WAIT)

            elif self.state == S1_WAIT:
                
                ## Create encoder 1 position variable to print
                positionPrint1 = self.enc_pos[0].get()
                ## Create encoder 2 position variable to print
                positionPrint2 = self.enc_pos[1].get()
                ## Create encoder 1 delta variable to print
                deltaPrint1 = self.enc_delta[0].get()
                ## Create encoder 2 delta variable to print
                deltaPrint2 = self.enc_delta[1].get()
                ## Create encoder 1 velocity variable to print
                
                
                if self.ser.any():
                    
                    ## Variable representing keyboard inputs to advance FSM
                    char_in = self.ser.read(1).decode()

                    # Zero the position of encoder 1
                    if(char_in == 'z'):
                        self.zero_flag[0].write(True)
                        print('Encoder 1 has been zeroed.')
                        
                    # Zero the position of encoder 2
                    if(char_in == 'Z'):
                        self.zero_flag[1].write(True)
                        print('Encoder 2 has been zeroed.')

                    # Print the position of encoder 1
                    elif(char_in == 'p'):
                        if(self.enc_pos[0].num_in()>0):
                            print('Encoder 1 Position: {}'.format(positionPrint1))
                    # Print the position of encoder 2
                    elif(char_in == 'P'):
                        if(self.enc_pos[1].num_in()>0):
                            print('Encoder 2 Position: {}'.format(positionPrint2))
                        
                    # Print the delta of encoder 1
                    elif(char_in == 'd'):
                        if(self.enc_delta[0].num_in()>0):
                            print('Encoder 1 Delta: {}'.format(deltaPrint1))
                    # Print the delta of encoder 2
                    elif(char_in == 'D'):
                        if(self.enc_delta[1].num_in()>0):
                            print('Encoder 2 Delta: {}'.format(deltaPrint2))
                            
                    # Collect encoder 1 data for 30 seconds
                    elif(char_in == 'g'):
                        self.transition_to(S2_COLLECT)
                        print('Collecting 30 seconds of encoder 1 data...')
                        
                    # Collect encoder 2 data for 30 seconds
                    elif(char_in == 'G'):
                        self.transition_to(S3_COLLECT)
                        print('Collecting 30 seconds of encoder 2 data...')
                        
                    # Duty cycle input prompt
                    elif(char_in == 'm'):
                        print('Enter motor 1 duty cycle: ')
                        self.transition_to(S14_DUTY)
                    
                    # Duty cycle input prompt
                    elif(char_in == 'M'):
                        print('Enter motor 2 duty cycle: ')
                        self.transition_to(S15_DUTY)
                    
                    # Speed control motor 1
                    elif(char_in == '1'):
                        print("Enter Kp value: ")
                        self.transition_to(S10_GET_KP)
                          
                    # Speed control motor 2
                    elif(char_in == '2'): 
                        print("Enter Kp value: ")
                        self.transition_to(S11_GET_KP)
                         
                    # Fault state
                    elif(char_in == 'f'):
                        print(self.fault_flag.read())
                                                
            elif self.state == S2_COLLECT:
                self.dataENC1[self.arrayIDX] = self.enc_pos[0].get()
                self.velENC1[self.arrayIDX] = self.enc_delta[0].get()
                self.dataENC2[self.arrayIDX] = self.enc_pos[1].get()
                self.velENC2[self.arrayIDX] = self.enc_delta[1].get()
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 300:
                    self.transition_to(S4_PRINT)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S4_PRINT)
                    elif(char_in == 'm'):
                        print('Enter motor 1 duty cycle: ')
                        duty1 = self.user_input()
                        self.duty[0].write(duty1)
                    
            elif self.state == S3_COLLECT:
                self.dataENC1[self.arrayIDX] = self.enc_pos[0].get()
                self.velENC1[self.arrayIDX] = self.enc_delta[0].get()
                self.dataENC2[self.arrayIDX] = self.enc_pos[1].get()
                self.velENC2[self.arrayIDX] = self.enc_delta[1].get()
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 300:
                    self.transition_to(S5_PRINT)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S5_PRINT)
                    elif(char_in == 'M'):
                        print('Enter motor 2 duty cycle: ')
                        duty2 = self.user_input()
                        self.duty[1].write(duty2)

            elif self.state == S4_PRINT:
                for forTime1,forENC1,forVel1 in zip(self.dataTime[:self.arrayIDX],
                                                    self.dataENC1[:self.arrayIDX],
                                                    self.velENC1):
                    print('Time: {}; Pos.: {}; Vel.: {}'.format(round(forTime1,3),forENC1,forVel1))
                self.transition_to(S0_INIT)
                self.timeEntry = 0
                self.arrayIDX = 0

            elif self.state == S5_PRINT:
                for forTime2,forENC2,forVel2 in zip(self.dataTime[:self.arrayIDX],
                                                    self.dataENC2[:self.arrayIDX],
                                                    self.velENC2):
                    #print('Time: {}; Pos.: {}; Vel.: {}'.format(round(forTime2,3),forENC2,forVel2))
                    print('{},{}'.format(round(forTime2,3),forVel2))
                self.transition_to(S0_INIT)
                self.timeEntry = 0
                self.arrayIDX = 0
                
            elif self.state == S6_COLLECT_CL:
                if self.enc_delta[0].num_in() > 0:
                    self.velENC1[self.arrayIDX] = self.enc_delta[0].get()
                if self.enc_delta[1].num_in() > 0:
                    self.velENC2[self.arrayIDX] = self.enc_delta[1].get()
                self.duty_data1[self.arrayIDX] = self.duty[0].read()
                self.duty_data2[self.arrayIDX] = self.duty[1].read()
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 100:
                    self.transition_to(S8_PRINT_CL)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S8_PRINT_CL)
                        
            elif self.state == S7_COLLECT_CL:
                if self.enc_delta[0].num_in() > 0:
                    self.velENC1[self.arrayIDX] = self.enc_delta[0].get()
                if self.enc_delta[1].num_in() > 0:
                    self.velENC2[self.arrayIDX] = self.enc_delta[1].get()
                self.duty_data1[self.arrayIDX] = self.duty[0].read()
                self.duty_data2[self.arrayIDX] = self.duty[1].read()
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 100:
                    self.transition_to(S9_PRINT_CL)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S9_PRINT_CL)
                
            elif self.state == S8_PRINT_CL:
                self.closedloop_flag[0].write(False)
                self.duty[0].write(0)
                for forTime3,forVel1,forDuty1 in zip(self.dataTime[:self.arrayIDX],
                                                     self.velENC1,
                                                     self.duty_data1):
                    #print('Time: {}; Vel.: {}, Duty: {}'.format(round(forTime3,3),forVel1,forDuty1))
                    print('{},{},{}'.format(round(forTime3,3),forVel1,forDuty1))
                self.timeEntry = 0
                self.arrayIDX = 0
                self.transition_to(S0_INIT)
                
            elif self.state == S9_PRINT_CL:
                self.closedloop_flag[1].write(False)
                self.duty[1].write(0)
                for forTime4,forVel2,forDuty2 in zip(self.dataTime[:self.arrayIDX],
                                                    self.velENC2,
                                                    self.duty_data2):
                    #print('Time: {}; Vel.: {}'.format(round(forTime3,3),forVel1,forDuty2))
                    print('{},{},{}'.format(round(forTime4,3),forVel2,forDuty2))
                self.timeEntry = 0
                self.arrayIDX = 0
                self.transition_to(S0_INIT)
            
            elif self.state == S10_GET_KP:
                Kp_1 = self.user_input()
                if (Kp_1 is not None):
                    self.Kp[0].write(Kp_1)
                    print("Enter a target velocity: ")
                    self.transition_to(S12_GET_VEL)
                    
            elif self.state == S11_GET_KP:
                Kp_2 = self.user_input()
                if (Kp_2 is not None):
                    self.Kp[1].write(Kp_2)
                    print("Enter a target velocity: ")
                    self.transition_to(S13_GET_VEL)          
            
            elif self.state == S12_GET_VEL:
                omega_ref1 = self.user_input()
                if (omega_ref1 is not None):
                    self.omega_ref[0].write(omega_ref1)
                    self.closedloop_flag[0].write(True)
                    self.transition_to(S6_COLLECT_CL) 
            
            elif self.state == S13_GET_VEL:
                omega_ref2 = self.user_input()
                if (omega_ref2 is not None):
                    self.omega_ref[1].write(omega_ref2)
                    self.closedloop_flag[1].write(True)
                    self.transition_to(S7_COLLECT_CL)
                    
            elif self.state == S14_DUTY:
                duty1 = self.user_input()
                if (duty1 is not None):
                    self.duty[0].write(duty1)
                    self.transition_to(S1_WAIT)
                    
            elif self.state == S15_DUTY:
                duty2 = self.user_input()
                if (duty2 is not None):
                    self.duty[1].write(duty2)
                    self.transition_to(S1_WAIT)
            
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.dataPeriod)
    
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state
    
    def user_input(self):
        '''@brief      Deals with user input 
           @details    Reads one character at a time from user
        '''
        # String which is printed into the GUI window
        number = None
        if self.ser.any():
            char_in = self.ser.read(1).decode()
            if char_in.isdigit():
                self.num_st += char_in
                sys.stdout.write(char_in)
                return None
            elif char_in == '-':
                if len(self.num_st) == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif ord(char_in) == 127: 
                if len(self.num_st) > 0: 
                    self.num_st = self.num_st[:-1]
                    sys.stdout.write('\b \b') 
                    sys.stdout.write('')
                    return None
            elif char_in == '.':
                if self.num_st.count('.') == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif char_in == '\r':
                print(' \n')
                number = float(self.num_st)
                self.num_st = ''
                return(number)
                