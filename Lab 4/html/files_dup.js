var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_enc.py", "task__enc_8py.html", "task__enc_8py" ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_safety.py", "task__safety_8py.html", "task__safety_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ]
];