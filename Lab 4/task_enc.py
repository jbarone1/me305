''' @file task_enc.py
    @brief Responsible for updating encoder object
    @details    Encoder is updateded at the requite interval to avoid
                missing any encoder ticks.
                Source code available here:
                https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
    @author Ryker Bullis
    @author Jack Barone
    @date October 16, 2021
'''

S0_WAIT = 0
import utime 
import math

class Task_Enc:   
    '''@brief Responsible for updating encoder object
       @details Encoder is updateded at the requite interval to 
       @avoid missing and encoder ticks
    '''
    def __init__(self,encoder,period,zero_flag,enc_pos,enc_delta): 
        '''@brief Constructs encoder task
           @details Task used to update encoder
           @param encoder       Encoder object imported from user
           @param period        Period that the encoder is updated at
           @param zero_flag     Flag used for zeroing encoder, shared 
                                between Task_Enc and Task_User
           @param enc_pos       Queue used for sharing encoder position data
                                between Task_Enc and Task_User
           @param enc_delta     Queue used for sharing encoder delta data
                                between Task_Enc and Task_User
        '''
    
        ## Encoder udates in this state
        self.state = S0_WAIT
        ## Runs used to count number of cycles of FSM
        self.runs = 0
        ## Period in micro seconds
        self.period = period
        ## Encoder object defined in main.py
        self.encoder = encoder
        ## Shared flag indicating a need to zero encoder
        self.zero_flag = zero_flag
        ## Shared queue for encoder positon
        self.enc_pos = enc_pos
        ## Shared queue for encoder delta 
        self.enc_delta = enc_delta
        ## Next time that the FSM should be running, fcn of the period
        self.next_time = utime.ticks_us() + period
        
    
    def run(self):
        '''@brief Runs an iteration of the FSM for updating encoder
        '''
        if(utime.ticks_us() >= self.next_time):
            if (self.state == S0_WAIT): 
                if(self.zero_flag.read() == True):
                    self.zero_flag.write(False) 
                    self.encoder.set_position(0)
                    self.encoder.update()
                else:
                    self.encoder.update()
            self.enc_pos.put(self.encoder.get_position()*math.pi/1000)
            self.enc_delta.put(self.encoder.get_delta()*math.pi/100)
            self.next_time += self.period   
            self.runs += 1

       
    def transition_to(self, new_state): 
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        ##Defines state as new_state
        self.state = new_state 
