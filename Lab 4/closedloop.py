''' @file closedloop.py
    @brief Implements a proportional speed controller for the motor
    @details Uses a proportional speed controller to set the speed of the 
    motor, accounting for the amount of error in speed compared to the 
    specified target velocity. 
    Source Code available here:
    https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
    @author Jack Barone
    @author Ryker Bullis
    @date November 15, 2021
'''

class ClosedLoop:
    ''' @brief Implements closed loop proportional speed control for motor
    '''
    def __init__(self, omega_ref, omega_meas, Kp):
        ''' @brief Initializes speed controller
            @details Uses reference velocity, measured velocity, and 
            proportional gain to set motor velocity.
            @param omega_ref        Reference velocity set by user
            @param omega_meas       Velocity measured by encoders
            @param Kp               Specified proportional gain
        '''
        self.omega_ref = omega_ref 
        self.omega_meas = omega_meas 
        self.Kp = Kp
        self.omega = 0
        
    def run(self):
        ''' @brief Run the closed loop speed controller
            @return Motor duty cycle
        '''
        if(self.omega_meas.num_in() > 0):
            self.omega = self.omega_meas.get()
        L = self.Kp.read() * (self.omega_ref.read() - self.omega)
        return(L)
    
    def get_Kp(self):
        ''' @brief Get the proportional gain
            @return Proportional gain, Kp
        '''
        return(self.Kp)
    
    def set_Kp(self,new_Kp):
        ''' @brief Set the new proportional gain
            @param New proportional gain
        '''
        self.Kp = new_Kp