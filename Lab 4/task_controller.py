''' @file task_controller.py
    @brief Responsible for motor control
    @details    Controls motor by submitting required duty cycle to speed 
                controller driver
                Source Code:
                https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
    @author Ryker Bullis
    @author Jack Barone
    @date November 15, 2021
'''

import closedloop
import utime

class Task_Controller:
    ''' @brief Motor speed controller used with driver for closed loop control
        @details    Sends required duty cycle to speed controller driver 
                    for proportional speed control
    '''

    def __init__(self, omega_meas, closedloop_flag, Kp, omega_ref, duty):
        ''' @brief Initializes speed controller
            @param omega_meas       Measured motor velocity from encoders
            @param closedloop_flag  Boolean variable which runs speed controller
            @param Kp               Proportional gain for motor control
            @param omega_ref        Target motor speed
            @param duty             Duty cycle of the motor
        '''
        self.omega_meas = omega_meas
        self.closedloop_flag = closedloop_flag
        self.Kp = Kp 
        self.omega_ref = omega_ref
        self.duty = duty
        self.dataPeriod = 100_000
        self.next_time = utime.ticks_add(utime.ticks_us(), self.dataPeriod) 
        self.ClosedLoopDriver = closedloop.ClosedLoop(self.omega_ref, self.omega_meas, self.Kp)
        
    def run(self):
        ''' @brief Runs the motor speed controller
        '''
        if self.closedloop_flag.read() == True: 
            self.duty.write(self.ClosedLoopDriver.run())
        