'''@file encoder.py
   @brief A driver for reading from Quadrature Encoders
   @details Source Code: https://bitbucket.org/rykerbullis/me305/src/master/Lab2/
   @author Ryker Bullis
   @author Jack Barone
   @date October 12, 2021
'''
import pyb

class Encoder:
    '''@brief Interface with quadrature encoders
       @details Python class for the encoder 
    '''
    def __init__(self,pin1,pin2,timeIDX):
        '''@brief Constructs an encoder object
           @details
        '''
        ## Largest period for which data can be collected on hardware
        self.period = 2**16-1
        ## Set up pyb timer
        self.tim = pyb.Timer(timeIDX,prescaler=0,period=self.period)
        ## Set up channel 1 for the encoder
        self.ch1 = self.tim.channel(1,pyb.Timer.ENC_A,pin=pin1)
        ## Set up channel 2 for the encoder
        self.ch2 = self.tim.channel(2,pyb.Timer.ENC_B,pin=pin2)
        ## Position of the encoder
        self.position = 0
        ## Change in position of the encoder
        self.delta = 0
        # Current time
        self.currentT = 0
        ## Last time
        self.lastT = 0
        
    def update(self):
        '''@brief Updates encoder position and delta
           @details
        '''
        
        self.currentT = self.tim.counter()
        self.delta = self.currentT - self.lastT
                
        if self.delta >= self.period/2:
            self.delta -= self.period
        elif self.delta <= -self.period/2:
            self.delta += self.period
        else:
            pass
                
        self.position += self.delta
        self.lastT = self.currentT

    def get_position(self):
        '''@brief Returns encoder position
           @details
           @return The position of the encoder shaft
        '''
        return self.position

    def set_position(self, position):
        '''@brief Sets encoder position
           @details
           @param position The new position of the encoder shaft
        '''
        self.position = position

    def get_delta(self):
        '''@brief Returns encoder delta
           @details
           @return The change in position of the encoder shaft
                    between the two most recent updates
        '''
        return self.delta