'''@file main.py
   @brief   main file for incremental encoder project
   @details Implements cooperative multitasking using tasks implemented by
            finite state machines.
            Link to FSM diagrams:
            https://cpslo-my.sharepoint.com/:f:/g/personal/jbarone_calpoly_edu/EgJWo3pkMp5Jk4R3YWhTAlYBa8rI6C3ZJtjnHclHs-RhxQ
            Source Code: https://bitbucket.org/rykerbullis/me305/src/master/Lab2/
   @author Jack Barone
   @author Ryker Bullis
   @date October 18, 2021
'''

import shares
import encoder
import pyb
import task_enc
import task_user

## Instantiates pin C6 on nucleo board
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
## Instantiates pin C7 on nucleo board
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
## Instantiates encoder used in task 1 
encB = encoder.Encoder(pinC6,pinC7,8) 

## Sets the period task_enc will run at
period_task_enc = 100_000
## Sets the period task_user will run at
data_period = 100_000

## Instantiates the shares.Queue object for encoder position 
enc_pos = shares.Queue()
## Instantiates the shares.Queue for encoder delta 
enc_delta = shares.Queue()
## Instantiates the shares.Share flag used for zeroing the encoder
zero_flag = shares.Share(False)

if __name__ == '__main__':
    
    ## An encoder task object 
    task1 = task_enc.Task_Enc(encB, period_task_enc, zero_flag, enc_pos, enc_delta)
    ## A user task object
    task2 = task_user.Task_User(data_period, zero_flag, enc_pos, enc_delta)
    
    while(True):
        try:
            task1.run()
            task2.run()
        except KeyboardInterrupt:
            break
    print('Program Terminating')