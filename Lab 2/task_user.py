'''@file task_user.py
   @brief User interface for encoder operation
   @details Implements a finite state machine for the user task
            Source Code: https://bitbucket.org/rykerbullis/me305/src/master/Lab2/
   @author Ryker Bullis
   @author Jack Barone
   @date October 16, 2021
'''
import utime
import pyb

S0_INIT             = 0
S1_WAIT             = 1
S2_COLLECT          = 2
S3_PRINT            = 3


class Task_User:
    ''' @brief User interface for encoder operation
        @details Implements a finite state machine for the user task
    '''
    
    def __init__(self,dataPeriod,zero_flag,enc_pos,enc_delta,dbg=True):
        '''@brief              Constructs a user task
           @details            The LED task is implemented as a finite state
                               machine.
           @param dataPeriod   Period for which data points are collected
           @param zero_flag    Shared object which resets encoder
           @param enc_pos      Shared queue for encoder position
           @param enc_delta    Shared queue for encoder delta
           @param dbg          Handles serial port error messages
        '''

        ## Set the initial state of the FSM
        self.state = S0_INIT       
        ## The period for checking commands
        self.dataPeriod = dataPeriod        
        ## Serial port to receive keystrokes in PuTTy
        self.ser = pyb.USB_VCP()        
        ## The time of the next FSM run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.dataPeriod)        
        ## Length of time to collect data
        self.dataLength = 30_000_000/dataPeriod        
        ## Create an empty list for the encoder position to record in
        self.dataENC = []
        ## Create an empty list for the encoder time to record in
        self.dataTime = [0]
        ## Define the zero flag for encoder 1
        self.zero_flag = zero_flag
        ## Current position of the encoder
        self.enc_pos = enc_pos        
        ## Delta of the encoder
        self.enc_delta = enc_delta
        # Create time step tracker variable
        self.timeEntry = 0
        
    def run(self):
        '''@brief Runs an iteration of the FSM
           @details Switches between waiting, collecting data, and printing.
        '''        
        ## Current time using the utime counter
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                print(' Press \'z\' to zero the encoder.\n',
                      'Press \'p\' to print the encoder position.\n',
                      'Press \'d\' to print the delta for the encoder.\n',
                      'Press \'g\' to collect 30 seconds of data.\n',
                      'Press \'s\' to stop data collection early.\n')
                self.transition_to(S1_WAIT)

            elif self.state == S1_WAIT:
                
                ## Create encoder position variable to print
                positionPrint = self.enc_pos.get()
                ## Create encoder delta variable to print
                deltaPrint = self.enc_delta.get()           
               
                if self.ser.any():
                    
                    ## Variable representing keyboard inputs to advance FSM
                    char_in = self.ser.read(1).decode()

                    # Zero the position of encoder
                    if(char_in == 'z' or char_in == 'Z'):
                        self.zero_flag.write(True)
                        print('Encoder has been zeroed.')

                    # Print the position of encoder
                    elif(char_in == 'p' or char_in == 'P'):
                        if(self.enc_pos.num_in()>0):
                            print('Position: {}'.format(positionPrint))
                        
                    # Print the delta of encoder
                    elif(char_in == 'd' or char_in == 'D'):
                        if(self.enc_delta.num_in()>0):
                            print('Delta: {}'.format(deltaPrint))
                            
                    # Collect data for 30 seconds
                    elif(char_in == 'g' or char_in == 'G'):
                        self.transition_to(S2_COLLECT)
                        print('Collecting 30 seconds of data...')
                        
            elif self.state == S2_COLLECT:
                self.dataENC.append(self.enc_pos.get())
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime.append(self.timeEntry)
                if len(self.dataENC) >= self.dataLength:
                    self.transition_to(S3_PRINT)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S3_PRINT)

            elif self.state == S3_PRINT:
                for forTime,forENC in zip(self.dataTime,self.dataENC):
                    print('Time: {}; Position: {}'.format(round(forTime,3),forENC))
                self.dataENC.clear()
                self.dataTime.clear()
                self.transition_to(S0_INIT)
                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.dataPeriod)
    
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state