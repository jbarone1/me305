'''@file                mainpage.py
   @brief               Page describing work done on Lab 1
                        
   @mainpage

   @section header1     Lab0x01 - Getting Started with Hardware
                        The purpose of this lab was to gain experience working 
                        with hardware in addition to Python. 
                        
   @section source1     Source code for Lab 1 is available here:
                        https://bitbucket.org/jbarone1/me305/src/master/Lab%201/
                        
   @section diagram1    State Transition Diagram 
                        State Transition Diagram for Lab0x01 found below
                        Diagram represents logic for LED to transition through 
                        different states and types of actions
                        
                        \image html FSMDiagramn-1.png "State Transition Diagram"
                        
   @section video1      Video
                        A video demonstration of the code can be seen here: https://youtu.be/0CtW0Ivbhq0   
'''