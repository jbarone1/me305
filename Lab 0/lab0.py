# -*- coding: utf-8 -*-
'''@file lab0.py
Created on Tue Sep 21 15:43:53 2021
@author: Jack Barone
'''

def fib(idx):
    def fib_memo(idx,m):    #defines memory bank for fibinacchi numbers 
                            # idx is the index for the desired fibonacci number
                            # m is the memory
        if idx < 0:         #checks for non-positive entries
            return ("Need a positive number")
        elif idx in m:      #checks to see this idx is stored in memory/m
            return m[idx]
        else:               #solves fibonacci number and stores it in memory/m
            solution = fib_memo(idx-1, m) + fib_memo(idx-2, m)
            m[idx] = solution
            return solution
    m = {0:1, 1:1}          #defines first 2 fib numbers
    return fib_memo(idx, m)
        
    
         
if __name__ == '__main__': 
    while True :
        string = input('Enter an index: ')
        try:
            idx = int(string)
        except:             #checks for non-integer entries
            print('The index must be an integer!')
            continue
        else: 
            fibnum = fib(idx)
            print ('Fibonacci number at ' 'index {:} is {:}.'. format(idx,fib(idx)))
        end = input('Enter q to quit or press enter to continue: ')
        if end == ('q') :
            break
        else: 
            continue 