import pyb

nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
nFAULT = pyb.Pin(pyb.Pin.cpu.B2)
IN1 = pyb.Pin(pyb.Pin.cpu.B4)
IN2 = pyb.Pin(pyb.Pin.cpu.B5)
IN3 = pyb.Pin(pyb.Pin.cpu.B0)
IN4 = pyb.Pin(pyb.Pin.cpu.B1)


tim3 = pyb.Timer(3, freq = 1000)
t3ch1 = tim3.channel(1,pyb.Timer.PWM,pin=IN1)
t3ch2 = tim3.channel(2,pyb.Timer.PWM,pin=IN2)
t3ch3 = tim3.channel(3,pyb.Timer.PWM,pin=IN3)
t3ch4 = tim3.channel(4,pyb.Timer.PWM,pin=IN4)
ser = pyb.USB_VCP()


if __name__ == '__main__':

    while(True):
        try:
            if ser.any():
                    char_in = ser.read(1).decode()
                    if(char_in == 'w' or char_in == 'W'):
                        nSLEEP.high()
                        t3ch1.pulse_width_percent(25)
                        t3ch2.pulse_width_percent(0)
                    elif(char_in == 's' or char_in == 'S'):
                        nSLEEP.high()
                        t3ch1.pulse_width_percent(0)
                        t3ch2.pulse_width_percent(25)
                    elif(char_in == 'q' or char_in == 'Q'):
                        nSLEEP.low()
                    elif(char_in == 'i' or char_in == 'I'):
                        nSLEEP.high()
                        t3ch3.pulse_width_percent(25)
                        t3ch4.pulse_width_percent(0)
                    elif(char_in == 'k' or char_in == 'K'):
                        nSLEEP.high()
                        t3ch3.pulse_width_percent(0)
                        t3ch4.pulse_width_percent(25)
                    elif(char_in == 'u' or char_in == 'U'):
                        nSLEEP.low()
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
            #t3ch3.pulse_width_percent(25)
            #t3ch4.pulse_width_percent(0)

        except KeyboardInterrupt:
            break

    #t3ch1.pulse_width_percent(100)
    #t3ch2.pulse_width_percent(100)
    #t3ch3.pulse_width_percent(100)
    #t3ch4.pulse_width_percent(100)
    print('Program Terminating')

    

