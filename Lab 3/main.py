'''@file main.py
   @brief       Main file for DC motor control lab
   @details     Implements motor driver, encoder driver, and various tasks
                to control the speed of two DC motors and record data
                Source code available here:
                https://bitbucket.org/rykerbullis/me305/src/master/Lab3/
                Motor Position and Velocity Graphs available here:
                https://cpslo-my.sharepoint.com/:f:/g/personal/rbullis_calpoly_edu/EuXnUm0ziY5InLq57a6uLY4BJFJz7WgRG9LXKc_D28FPew?e=biscrd
   @author Jack Barone
   @author Ryker Bullis
   @date November 1, 2021                
'''
import pyb
import DRV8847
import encoder
import shares 
import task_user
import task_enc
import task_motor
import task_safety

## Instantiates pin B6 on nucleo board
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Instantiates pin B7 on nucleo board
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
## Instantiates pin C6 on nucleo board
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
## Instantiates pin C7 on nucleo board
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

## Instantiates encoder used in task 1
encA = encoder.Encoder(pinB6,pinB7,4)
## Instantiates encoder used in task 2
encB = encoder.Encoder(pinC6,pinC7,8)

## Instantiates fault_flag as a share so it can be accessed by applicable tasks
fault_flag = shares.Share(False)

## Instantiates pin associated with hardware faults
fault_pin = pyb.Pin.cpu.B2

## Instantiates motor driver by calling motor driver function
motor_drv = DRV8847.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP,fault_pin,fault_flag,3,20000)

## Instantiates pin used for control of motor 1
IN1 = pyb.Pin(pyb.Pin.cpu.B4)
## Instantiates second pin used for control of motor 1
IN2 = pyb.Pin(pyb.Pin.cpu.B5)
## Instantiates pin used for control of motor 2
IN3 = pyb.Pin(pyb.Pin.cpu.B0)
## Instantiates second pin used for control of motor 2
IN4 = pyb.Pin(pyb.Pin.cpu.B1)
## Instantiates pin associated with fault state
nFAULT = pyb.Pin(pyb.Pin.cpu.B2)

## Sets the period task_enc will run at
period_task_enc = 100_000
## Sets the period task_user will run at
data_period = 100_000

# Instantiates the shares.Queue tuple for encoder position 
enc_pos = (shares.Queue(),shares.Queue())
# Instantiates the shares.Queue tuple for encoder delta 
enc_delta = (shares.Queue(),shares.Queue())
# Instantiates the shares.Share tuple used for zeroing the encoder
zero_flag = (shares.Share(False),shares.Share(False))
## Instantiates the shares tuple used to share the motor duty cycle
duty = (shares.Share(0),shares.Share(0))

## Instantiate motor 1
motor_1 = motor_drv.motor(IN1, 1, IN2, 2)
## Instantiate motor 2
motor_2 = motor_drv.motor(IN3, 3, IN4, 4)

if __name__ == '__main__':
     task1 = task_enc.Task_Enc(encA, period_task_enc, zero_flag[0], enc_pos[0], enc_delta[0])
     task2 = task_enc.Task_Enc(encB, period_task_enc, zero_flag[1], enc_pos[1], enc_delta[1])
     task3 = task_user.Task_User(data_period, zero_flag, enc_pos, enc_delta, fault_flag, duty)
     task4 = task_motor.Task_Motor(fault_flag,motor_drv,duty[0], motor_1)
     task5 = task_motor.Task_Motor(fault_flag,motor_drv,duty[1], motor_2)
     task6 = task_safety.Task_Safety(fault_flag,motor_drv)
     while(True): 
         try:
             task1.run()
             task2.run()
             task3.run()
             task4.run()
             task5.run()
             task6.run()
         except KeyboardInterrupt:
             break 
     print('Program Terminating')