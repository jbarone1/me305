'''@file task_safety.py
   @brief Safety task for handling fault
   @details    Detects faults triggered by hardware
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab3/
   @author Ryker Bullis
   @author Jack Barone
   @date October 25, 2021
'''
import pyb

S0_WAIT     = 0
S1_FAULT    = 1

class Task_Safety:   
    '''@brief Responsible for updating the fault flag
       @details    Disables both motors when a fault is detected under a true
                   fault flag, then prompts the user to clear the fault with
                   a keyboard input
    '''
    def __init__(self,fault_flag,motor_drv): 
        '''@brief Constructs safety task
           @details Task used to handle faults
           @param fault_flag    Boolean variable indicating if a fault is detected
           @param motor_drv     Motor driver
        '''
        ## Initial state for task safety
        self.state = S0_WAIT
        ## Boolean object used to indicate faults
        self.fault_flag = fault_flag
        ## Setup for virtual comm port
        self.ser = pyb.USB_VCP()
        ## Motor driver object
        self.motor_drv = motor_drv

    def run(self):
        '''@brief Runs the safety task
        '''
        if self.state == S0_WAIT:
            if(self.fault_flag.read() == True):
                self.motor_drv.disable()
                print('Fault detection triggered, system powering down. \n',
                      'Press c to clear.')
                self.transition_to(S1_FAULT)
            
        if self.state == S1_FAULT:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'c' or char_in == 'C'):
                        print('Fault cleared.')
                        self.fault_flag.write(False)
                        self.motor_drv.enable()
                        self.transition_to(S0_WAIT)
    
    def transition_to(self, new_state): 
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        ##Defines state as new_state
        self.state = new_state 