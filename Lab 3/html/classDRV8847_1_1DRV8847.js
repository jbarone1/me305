var classDRV8847_1_1DRV8847 =
[
    [ "__init__", "classDRV8847_1_1DRV8847.html#a69ee44815492a7a852c940ce5fe71dfa", null ],
    [ "disable", "classDRV8847_1_1DRV8847.html#acd9dbef9212b3014eab18a57a6e0f13a", null ],
    [ "enable", "classDRV8847_1_1DRV8847.html#a57adaca9549fa5935979ff8f0bcdda13", null ],
    [ "fault_cb", "classDRV8847_1_1DRV8847.html#a1b42a1831c397b58546a4261afd621ec", null ],
    [ "motor", "classDRV8847_1_1DRV8847.html#a693f0ec0b106c56cd6417498878cfb70", null ],
    [ "fault_flag", "classDRV8847_1_1DRV8847.html#a586a64971e5f017890f16655b62b5b58", null ],
    [ "fault_pin", "classDRV8847_1_1DRV8847.html#a551694fb681eb70fb78bcdc79f86265d", null ],
    [ "nSLEEP", "classDRV8847_1_1DRV8847.html#aedae22eb42efbed4ef2608c52ff1e43a", null ],
    [ "TIMER", "classDRV8847_1_1DRV8847.html#ae492b7d0c86e595787fddd3b2f827969", null ]
];