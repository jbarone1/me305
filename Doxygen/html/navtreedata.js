/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Portfolio", "index.html", [
    [ "Welcome", "index.html#wel", null ],
    [ "Source Code", "index.html#source", null ],
    [ "Note", "index.html#note", null ],
    [ "Homework0x02", "HW2.html", [
      [ "Overview", "HW2.html#analysis", null ],
      [ "Analysis", "HW2.html#diagrams", null ]
    ] ],
    [ "Homework0x03", "HW3.html", [
      [ "Overview", "HW3.html#overview", null ],
      [ "Open Loop Response", "HW3.html#open", null ],
      [ "Closed Loop Response", "HW3.html#closed", null ]
    ] ],
    [ "Lab0x00", "00.html", [
      [ "Lab0x00 - Fibonacci Sequence", "00.html#header0", null ],
      [ "Source Code for Lab 0 is available here:", "00.html#source0", null ]
    ] ],
    [ "Lab0x01", "01.html", [
      [ "Lab0x01 - Getting Started with Hardware", "01.html#header1", null ],
      [ "Formal Documentation", "01.html#doc1", null ],
      [ "Source code for Lab 1 is available here:", "01.html#source1", null ],
      [ "State Transition Diagram and Video", "01.html#diagram1", null ],
      [ "Video", "01.html#video1", null ]
    ] ],
    [ "Lab0x02", "02.html", [
      [ "Lab0x02 - Incremental Encoders", "02.html#header2", null ],
      [ "Formal Documentation", "02.html#doc2", null ],
      [ "Source code for Lab 2 is available here:", "02.html#source2", null ],
      [ "State Transition and Task Diagrams", "02.html#diagrams2", null ]
    ] ],
    [ "Lab0x03", "03.html", [
      [ "Lab0x03 - DC Motor Control", "03.html#header3", null ],
      [ "Formal Documentation", "03.html#doc3", null ],
      [ "Source code for Lab 3 is available here:", "03.html#source3", null ],
      [ "Motor Plots", "03.html#plots3", null ]
    ] ],
    [ "Lab0x04", "04.html", [
      [ "Lab0x04 - Closed Loop Speed Control", "04.html#header4", null ],
      [ "Formal Documentation", "04.html#doc4", null ],
      [ "Source Code", "04.html#source4", null ],
      [ "Task Diagrams and Tuning Plots", "04.html#diagrams4", null ]
    ] ],
    [ "Lab0x05", "05.html", [
      [ "Lab0x05 - I2C and Inertial Measurement Units", "05.html#header5", null ],
      [ "Source Code", "05.html#source5", null ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"00.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';