/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "HW2and3", "index.html", [
    [ "Welcome", "index.html#wel", null ],
    [ "Source Code", "index.html#source", null ],
    [ "Homework0x02", "02.html", [
      [ "Overview", "02.html#analysis", null ],
      [ "Analysis", "02.html#diagrams", null ]
    ] ],
    [ "Homework0x03", "03.html", [
      [ "Overview", "03.html#overview", null ],
      [ "Open Loop Response", "03.html#open", null ],
      [ "Closed Loop Response", "03.html#closed", null ]
    ] ],
    [ "Lab0x04", "04.html", [
      [ "Lab0x04 - Closed Loop Speed Control", "04.html#header", null ],
      [ "Formal Documentation", "04.html#doc4", null ],
      [ "Introduction", "04.html#sec_intro", null ],
      [ "Motor Driver", "04.html#sec_DRV8847", null ],
      [ "Encoder Driver", "04.html#sec_enc", null ],
      [ "Motor Task", "04.html#sec_mot", null ],
      [ "Encoder Task", "04.html#sec_taskenc", null ],
      [ "Closed Loop Controller", "04.html#sec_cl", null ],
      [ "Share", "04.html#sec_share", null ],
      [ "Task Controller", "04.html#sec_con", null ],
      [ "Safety Task", "04.html#sec_safety", null ],
      [ "User Task", "04.html#sec_user", null ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"02.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';