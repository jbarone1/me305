import pyb
import time

pinA0 = pyb.Pin(pyb.Pin.cpu.A0)
pinA1 = pyb.Pin(pyb.Pin.cpu.A1)
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)
xp = pyb.Pin(pinA7,mode=pyb.Pin.OUT_PP)
xm = pyb.Pin(pinA1,mode=pyb.Pin.OUT_PP)

if __name__ == '__main__':
    while True:
        try:

            xp.init(mode=pyb.Pin.OUT_PP)
            xm.init(mode=pyb.Pin.OUT_PP)
            
            xp.high()
            xm.low() 
            
            yp = pyb.Pin(pinA0,mode=pyb.Pin.IN)
            ym = pyb.Pin(pinA6,mode=pyb.Pin.IN)
            
            adc_ym = pyb.ADC(pinA0)
            ym_val = adc_ym.read()
            x = ym_val*176/4095 - 88
            
            time.sleep(0.1)
            
            yp = pyb.Pin(pinA6,mode=pyb.Pin.OUT_PP)
            ym = pyb.Pin(pinA0,mode=pyb.Pin.OUT_PP)
            
            yp.high() 
            ym.low() 
            
            xp = pyb.Pin(pinA7,mode=pyb.Pin.IN)
            xm = pyb.Pin(pinA1,mode=pyb.Pin.IN)
            
            adc_xm = pyb.ADC(pinA7)
            xm_val = adc_xm.read() 
            y = xm_val*100/4095 - 50
            
            yp = pyb.Pin(pinA6,mode=pyb.Pin.OUT_PP)
            xm = pyb.Pin(pinA1,mode=pyb.Pin.OUT_PP)
            
            yp.high()
            xm.low() 
            adc_cont = pyb.ADC(pinA0)
            cont_val = adc_cont.read() 
            
            if 40 <= cont_val <= 4055:
                contact = True
            else:
                contact = False
            
            print('x: {}, y: {}, Cont: {}'.format(x,y,contact))
        except KeyboardInterrupt:
            break