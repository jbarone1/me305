import pyb
import time
import touchpanel
import os

pinA0 = pyb.Pin(pyb.Pin.cpu.A0)
pinA1 = pyb.Pin(pyb.Pin.cpu.A1)
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)

xp = pyb.Pin(pinA7,mode=pyb.Pin.OUT_PP)
xm = pyb.Pin(pinA1,mode=pyb.Pin.OUT_PP)
yp = pyb.Pin(pinA0,mode=pyb.Pin.IN)
ym = pyb.Pin(pinA6,mode=pyb.Pin.IN)
w = 176
l = 100


if __name__ == '__main__':
    
    task1 = touchpanel.TouchPanel(pinA0,pinA7, xp,xm,yp,ym,w,l)
    
    filename = "RT_cal_coeffs.txt"
    if filename in os.listdir():
        with open(filename,'r') as f:
            cal_data_string = f.readline()      
            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
            task1.writeConstants(cal_values)
    else:
        with open(filename, 'w') as f:  
            (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = task1.calibration()
            f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
            
    while True:
        try:
            print(task1.scanXY())
            time.sleep(0.5)
        except KeyboardInterrupt:
            break